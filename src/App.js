import React, { Component } from 'react';
// import components
import ChatRoom from './components/containers/ChatRoom'
// css
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        <ChatRoom />
      </div>
    );
  }
}

export default App;
