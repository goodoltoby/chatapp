import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/actions/chatActions';
import * as uiActions from '../../redux/actions/uiActions';
// components
import RoomList from '../RoomList'
import UserList from '../UserList'
import MessageList from '../MessageList'
import AddRoomForm from '../AddRoomForm'
import SendMsgForm from '../SendMsgForm'

class ChatRoom extends Component {
  constructor(props) {
    super(props);
    
    this.subscribeToRoom = this.subscribeToRoom.bind(this)  
    this.handleToggle = this.handleToggle.bind(this)

  }  

  componentDidMount(){
    const { initChatManager } = this.props;  
    // pass logged in user as argument when BE is implemented
    initChatManager('goodoltoby')
  }

  componentDidUpdate(prevProps){
    const { refreshRoom, updateRooms } = this.props,
          { user, currentRoomId, createdRoomId } = this.props.chat
    
    // when current user is set in store
    if(prevProps.chat.user !== user)
      updateRooms(user)

    // when we get a new subscribed room
    if(prevProps.chat.currentRoomId !== currentRoomId){
      refreshRoom()
    }

    if(prevProps.chat.createdRoomId !== createdRoomId && currentRoomId !== createdRoomId){
      this.subscribeToRoom(createdRoomId)
      updateRooms(user)
    }
  }
  
  // subscribes to room
  subscribeToRoom(roomId){
    const { setRoomMsgs, setCurrentRoomId, setCurrentRoomUsers } = this.props,
          { user } = this.props.chat;

    if(roomId)
      setCurrentRoomId(roomId);

    user.subscribeToRoom({
      roomId: roomId,
      hooks: {
        onNewMessage: message => {
          setRoomMsgs(message)
        }
      },    
    })
    .then( room => setCurrentRoomUsers(room.users) )
    .catch(err => {
      console.log('Error on subscribe to room', err)
    })    
  }

  handleToggle(){
    const { toggleControllPanel } = this.props,
          { toggledPanel } = this.props.ui;

    toggleControllPanel(!toggledPanel);
  }    

  render() {
    const { toggledPanel } = this.props.ui;

    const renderToggleBtn = (
      <span
        onClick={ this.handleToggle } 
        className={ "ChatRoom__toggleBtn " + (toggledPanel ? 'ChatRoom__toggleBtn--closed' : null) }
      >
        { toggledPanel ? '-' : '+' }
      </span>
    )

    return (
      <section className={"ChatRoom " + (toggledPanel ? 'ChatRoom__toggled' : null)}>
        <div className="ChatRoom__controllpanel">
          <UserList { ...this.props } />
          <RoomList 
            { ...this.props }
            joinRoom={ this.subscribeToRoom }
          />        
          <AddRoomForm { ...this.props } />        
        </div>
        <div className="ChatRoom__chat">
          { renderToggleBtn }
          <MessageList { ...this.props }/>
          <SendMsgForm
            { ...this.props }
          />        
        </div>                
      </section>
    );
  }
}

const mapStateToProps = state => ({
  chat: state.chat,
  ui: state.ui,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...uiActions, ...chatActions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ChatRoom);