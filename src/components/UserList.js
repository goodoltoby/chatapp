import React from 'react';
// components
import User from './User'

const UserList = (props) => {
	const { currentRoomUsers } = props.chat;
  return (
    <section className="UserList">
      <span className="UserList__topic">All users:</span>
      { 
      	currentRoomUsers.map((user, index) => {
					return(
						<User 
							key={ index }
							name={ user.id }
						/>
					)
      	}) 
    	}
    </section>
  );
}

export default UserList;