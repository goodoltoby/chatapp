import React from 'react';
// components
import Room from './Room'

const RoomList = (props) => {	
	const { rooms, currentRoomId } = props.chat,
				{ joinRoom } = props;
        
  return (  	
    <div className="RoomList">
      <span className="RoomList__topic">All rooms:</span>    	
      {
      	rooms.map((room, index) => {
      		return(
						<Room 
							key={ room.id }
							activeClass={ currentRoomId === room.id ? 'active' : null } 
							handleClick={ () => joinRoom(room.id) }
							name={ room.name }							
						/> 
      		)
      	})
      }
    </div>
  )
}

export default RoomList;
