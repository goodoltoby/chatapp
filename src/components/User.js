import React from 'react';

const User = (props) => {
	const { name } = props;

  return (
    <div className="User">
      <span className="User__name">{ name }</span>
    </div>
  );
}

export default User;