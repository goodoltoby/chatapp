import React, { Component } from 'react';

class SendMsgForm extends Component {
    constructor(props) {
      super(props);

      this.state = {
				message: ''
      }
			
			this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e){  	
		this.setState({
			message: e.target.value
		});
  }

  handleSubmit(e){
    e.preventDefault();
    
  	const { sendMessage } = this.props,
          { user, currentRoomId } = this.props.chat,
  				{ message } 		= this.state;            

  	if(message){
	  	sendMessage(user, currentRoomId, message);
	  	this.setState({
	  		message: ''
	  	})
  	}
  }

  render() {
    const { currentRoomId } = this.props.chat;

    return (
      <form
      	onSubmit={ this.handleSubmit }
        className="SendMsgForm"
      >
	      <input
	      	onChange={ this.handleChange }
	      	value={ this.state.message }
	      	className="SendMsgForm__input"
	        placeholder={ currentRoomId ? 'Enter message here!' : 'Select a room!'}
	        type="text"
          disabled={ !currentRoomId ? 'disabled' : null }
	      />
        <input 
          className={ 'SendMsgForm__submit ' + (currentRoomId ? 'SendMsgForm__submit--active' : 'SendMsgForm__submit--inactive') }           
          type="submit" 
          value={ currentRoomId ? 'Submit' : ':)' } 
        />
      </form>
    );
  }
}

export default SendMsgForm;
