import React, { Component } from 'react';
import ReactDOM from 'react-dom'
// components
import Message from './Message'

class MessageList extends Component {	
  componentWillUpdate() {
		this.scrollCheck()
  }
  
  componentDidUpdate() {
		this.scrollToBottom()
  }

  scrollCheck(){
    const node = ReactDOM.findDOMNode(this),
    			buffer = 200;
    this.shouldScrollToBottom = node.scrollTop + node.clientHeight + buffer >= node.scrollHeight  	
  }

  scrollToBottom(){
    const node = ReactDOM.findDOMNode(this)
    
    if(this.shouldScrollToBottom)
    	node.scrollTop = node.scrollHeight
  }

	render() {
		const { roomMessages, currentRoomId } = this.props.chat;

		const mapMessages = roomMessages.map((message, index) => {       		
			return (
				<Message 
					key={ message.id }
					username={ message.senderId }
					text={ message.text }
					created={ message.createdAt }
				/>	
			)
  	})

	  return (
	    <section className="MessageList">
	    	{ 
	    		currentRoomId ?
	    			mapMessages 
	    		:
						<div className="MessageList__greetings">
							<h2 className="MessageList__greetings--topic">Greetings!</h2>
							<p className="MessageList__greetings--message">Select or Create a room to get started with your chatting! :)</p>
						</div>	    		
	    	}	    		      
		      
	    </section>
	  );
	}
}

export default MessageList;