import React, { Component } from 'react';

class AddRoomForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
    	roomName: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(e){
		this.setState({
			roomName: e.target.value
		})
  }

  handleSubmit(e){
    e.preventDefault();

  	const { createRoom } = this.props,
          { user } = this.props.chat,
  				{ roomName } = this.state;

		if(roomName){
    	createRoom(user, roomName);
    	this.setState({roomName: ''});
		}		
  }  

  render() {
    return (
      <form
      	onSubmit={ this.handleSubmit }
        className="AddRoomForm"
      >
	      <input
	      	onChange={ this.handleChange }
	      	value={ this.state.roomName }
	      	className="AddRoomForm__input"
	        placeholder="Add room"
	        type="text" 
	      />
      </form>
    );
  }
}

export default AddRoomForm;
