import React from 'react';

const Message = (props) => {
	const { username, text, created } = props,
				date = new Date(created),
				format_date = date.toLocaleString();

  return (
		<div className="Message">
			<span className="Message__user"> { username } </span>
			<span className="Message__text"> { text } </span>
			<span className="Message__created"> { format_date } </span>
		</div>
  );
}

export default Message;
