import React from 'react';

const Room = (props) => {
	const { handleClick, name, activeClass } = props;
  return (
		<div 
			className={ "Room " + activeClass }
			onClick={ handleClick }
		>
			<span>{ name }</span>
		</div>
  );
}

export default Room;
