// chat actions
export const SET_ROOM_MSGS = 'SET_ROOM_MSGS'
export const SET_USER = 'SET_USER'
export const SET_ROOMS = 'SET_ROOMS'
export const SET_CURRENT_ROOM_ID = 'SET_CURRENT_ROOM_ID'
export const SET_CURRENT_ROOM_USERS = 'SET_CURRENT_ROOM_USERS'
export const RESET_ROOM_MSGS = 'RESET_ROOM_MSGS'
export const RESET_CURRENT_ROOM_USERS = 'RESET_CURRENT_ROOM_USERS'
export const RESET_ROOMS = 'RESET_ROOMS'
export const MESSAGE_SENT = 'MESSAGE_SENT'
export const CREATED_ROOM_ID = 'CREATED_ROOM_ID'


// ui actions
export const TOGGLE_CONTROLLPANEL = 'TOGGLE_CONTROLLPANEL'
