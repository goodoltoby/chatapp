// import actionTypes
import { TOGGLE_CONTROLLPANEL } from '../constants/actionTypes'

export const toggleControllPanel = (bool) => dispatch => {
	dispatch({
		type: TOGGLE_CONTROLLPANEL,
		payload: bool
	})
}