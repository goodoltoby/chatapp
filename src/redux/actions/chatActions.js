// Chatkit SDK
import { ChatManager, TokenProvider } from '@pusher/chatkit'
// import actionTypes
import { SET_ROOM_MSGS } from '../constants/actionTypes'
import { SET_USER } from '../constants/actionTypes'
import { SET_ROOMS } from '../constants/actionTypes'
import { SET_CURRENT_ROOM_ID } from '../constants/actionTypes'
import { SET_CURRENT_ROOM_USERS } from '../constants/actionTypes'
import { RESET_ROOM_MSGS } from '../constants/actionTypes'
import { RESET_CURRENT_ROOM_USERS } from '../constants/actionTypes'
import { RESET_ROOMS } from '../constants/actionTypes'
import { MESSAGE_SENT } from '../constants/actionTypes'
import { CREATED_ROOM_ID } from '../constants/actionTypes'

const chatManager = (userId) => 
	new ChatManager({
	  instanceLocator: process.env.REACT_APP_INSTANCE_LOCATOR,
	  userId: userId,
	  tokenProvider: new TokenProvider({
	    url: process.env.REACT_APP_TOKEN_PROVIDER
	  })
	});

export const initChatManager = (userId) => dispatch => {
	chatManager(userId)
		.connect()
		.then(currentUser => {
			dispatch({ type: SET_USER, payload: currentUser })
		})
    .catch(err => {
      console.log('Error on connection', err)
    })		
}

export const updateRooms = (user) => dispatch => {
  user.getJoinableRooms()
  .then(dispatch({type: RESET_ROOMS, payload: []}))
  .then(joinableRooms => {
    dispatch({ type: SET_ROOMS, payload: user.rooms, joinableRooms });
  })
  .catch(err => console.log('error on joinableRooms: ', err))	
}

export const refreshRoom = () => dispatch => {
	dispatch({ type: RESET_ROOM_MSGS, payload: [] })
	dispatch({ type: RESET_CURRENT_ROOM_USERS, payload: [] })
}

export const sendMessage = (user, roomId, text) => dispatch => {
	user.sendMessage({
	  text,
	  roomId
	})
	.then({ type: MESSAGE_SENT, payload: true })
	.catch(err => {
	  console.log('Error on send message', err)
	})
}

export const createRoom = (user, roomName) => dispatch => {
  user.createRoom({
    name: roomName
  })
  .then(room => dispatch({ type: CREATED_ROOM_ID, payload: room.id }))
  .catch(err => console.log('error with create room: ', err))
}

export const setRoomMsgs = (messages) => dispatch => {
	dispatch({
		type: SET_ROOM_MSGS,
		payload: messages
	})
}

export const setCurrentRoomId = (currentRoomId) => dispatch => {
	dispatch({
		type: SET_CURRENT_ROOM_ID,
		payload: currentRoomId
	})
}

export const setCurrentRoomUsers = (users) => dispatch => {
	dispatch({
		type: SET_CURRENT_ROOM_USERS,
		payload: users
	})
}

export const resetRoomMsgs = () => dispatch => {
	dispatch({
		type: RESET_ROOM_MSGS,
		payload: []
	})
}

export const resetCurrentRoomUsers = () => dispatch => {
	dispatch({
		type: RESET_CURRENT_ROOM_USERS,
		payload: []
	})
}

export const resetRooms = () => dispatch => {
	dispatch({
		type: RESET_ROOMS,
		payload: []
	})
}