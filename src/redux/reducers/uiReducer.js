import * as ActionTypes from '../constants/actionTypes'

const initialState = {
	toggledPanel: false
}

export default (state = initialState, action) => {
	switch (action.type) {
		case ActionTypes.TOGGLE_CONTROLLPANEL:
			return {
				...state,
				toggledPanel: action.payload
			}																							
		default:
			return state
 }
}