import * as ActionTypes from '../constants/actionTypes'

const initialState = {
	roomMessages: [],
	user: {},
	rooms: [],
	currentRoomId: '',
	currentRoomUsers: [],
	toggledPanel: false,
	messageSent: false,
	createdRoomId: ''
}

export default (state = initialState, action) => {
	switch (action.type) {
		case ActionTypes.SET_ROOM_MSGS:
			return {
				...state,
				roomMessages: [...state.roomMessages, action.payload]
			}
		case ActionTypes.RESET_ROOM_MSGS:
			return {
				...state,
				roomMessages: []
			}			
		case ActionTypes.SET_USER:
			return {
				...state,
				user: action.payload
			}	
		case ActionTypes.SET_ROOMS:
			return {
				...state,
				rooms: [...state.rooms, ...action.payload]
			}
		case ActionTypes.SET_CURRENT_ROOM_ID:
			return {
				...state,
				currentRoomId: action.payload
			}
		case ActionTypes.SET_CURRENT_ROOM_USERS:
			return {
				...state,
				currentRoomUsers: [...state.currentRoomUsers, ...action.payload]
			}
		case ActionTypes.RESET_CURRENT_ROOM_USERS:
			return {
				...state,
				currentRoomUsers: []
			}
		case ActionTypes.RESET_ROOMS:
			return {
				...state,
				rooms: []
			}
		case ActionTypes.MESSAGE_SENT:
			return {
				...state,
				messageSent: action.payload
			}
		case ActionTypes.CREATED_ROOM_ID:
			return {
				...state,
				createdRoomId: action.payload
			}			
		default:
			return state
 }
}