import { combineReducers } from 'redux';
// reducers
import chatReducer from './chatReducer';
import uiReducer from './uiReducer';

export default combineReducers({
	chat: chatReducer,
	ui: uiReducer
});